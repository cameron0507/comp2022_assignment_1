#! /bin/sh

echo "Beginning Testing."

echo "Section1: ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
for i in $(seq 0 256);
do
 T1=$(echo "obase=2;$i" | bc)
 T2=$(java MyProgram $T1 aBabORbaBbS.txt | tail -n 1)
 T3=$(echo $T1 | egrep -x "1(10|01)0*")
 if [ "$T3" = "$T1" ]; then
     T4="accepted"
 else
     T4="rejected"
 fi
 if [ "$T2" = "$T4" ]; then
     echo "$T1: passed test Section1"
 else
     echo "$T1: failed test Section1"
     echo "T1 = $T1, T3 = $T3"
 fi
 echo "===================="
done

echo "Section2: ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
for i in $(seq 0 256);
do
    T1=$(echo "obase=2;$i" | bc)
    T2=$(java MyProgram $T1 task3Binary.txt | tail -n 1)
    T3=$(echo $T1 | egrep -x "((0*1)*)|((101)+0)|(0)")
    if [ "$T3" = "$T1" ]; then
        T4="accepted"
    else
        T4="rejected"
    fi
    if [ "$T2" = "$T4" ]; then
        echo "$T1: passed test Section2"
    else
        echo "$T1: failed test Section2"
        echo "T1 = $T1, T3 = $T3"
    fi
    echo "===================="
done

echo "Section3: ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
for i in $(seq 0 256);
do
    T1=$(echo "obase=2;$i" | bc | sed s/"^1"/"01"/g)
    T2=$(java MyProgram $T1 task3Binary.txt | tail -n 1)
    T3=$(echo $T1 | egrep -x "((0*1)*)|((101)+0)|(0)")
    if [ "$T3" = "$T1" ]; then
        T4="accepted"
    else
        T4="rejected"
    fi
    if [ "$T2" = "$T4" ]; then
        echo "$T1: passed test Section2"
    else
        echo "$T1: failed test Section2"
        echo "T1 = $T1, T3 = $T3"
    fi
    echo "===================="
done

echo "Finished Testing."
