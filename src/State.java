public class State {
    String name;
    Boolean isAccept;
    String[] transitionFunction;

    //Constructor
    public State(String name, boolean isAccept) {
        this.name = name;
        this.isAccept = isAccept;
    }

    public void setTransitionFuntion(String[] alphabet, String[] outputs) {
        transitionFunction = new String[alphabet.length];
        for (int i = 0; i < transitionFunction.length; i++) {
            transitionFunction[i] = outputs[i];
        }
    }

    // This function takes an input from the alphabet and returns the name of
    //  the state to transition to.
    public String transitionFunction(String input, String[] alphabet) {
        for (int i = 0; i < transitionFunction.length; i++) {
            if (alphabet[i].equals(input)) {
                return transitionFunction[i];
            }
        }
        // If this code reached, something has gone wrong.
        return null;
    }
}
