import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.ArrayList;
//import State.class; Dont need to import classes in same package

public class MyProgram {

    // ArrayList used to store the DFA.
    static ArrayList<State> dfa = new ArrayList<State>();

    // Array used to store the alphabet.
    static String[] alphabet;

    // Current state.
    static State current;

    // Build the DFA based on input file, or default DFA if none given.
    public static void buildDFA(File file) throws FileNotFoundException {
            Scanner scan = new Scanner(file);

        // 1st line contains states separated by commas
        String firstLine = scan.nextLine();
        String[] states = firstLine.split(",");
        for (String state : states) {
            dfa.add(new State(state, false));
        }

        // 2nd line contains the alphabet
        String secondLine = scan.nextLine();
        alphabet = secondLine.split(",");

        // 3rd line contains the start state;
        String thirdLine = scan.nextLine();
        for (State state : dfa) {
            if (state.name.equals(thirdLine)) {
                current = state;
                break;
            } else {
                continue;
            }
        }

        // 4th line contains list of accepting states
        String fourthLine = scan.nextLine();
        String[] acceptStates = fourthLine.split(",");
        for (String acceptState : acceptStates) {
            for (State state : dfa) {
                if (state.name.equals(acceptState)) {
                    state.isAccept = true;
                    break;
                } else {
                    continue;
                }
            }
        }

        // Loop from 5th line till end to fill out transition functions.
        // For correct input, there should be as many lines here as there is
        //  states in the DFA. Add error checking for incorrect input later?
        for (State state : dfa) {
            String nextLine = scan.nextLine();
            String[] outputs = nextLine.split(",");
            state.setTransitionFuntion(alphabet, outputs);
        }

        scan.close();

    }

    public static void createDefaultDFAFile() throws FileNotFoundException,
                                                UnsupportedEncodingException {
        // Creating file with default minimal DFA to read from.
        PrintWriter writer = new PrintWriter("minDFA.txt", "UTF-8");
        writer.println("q0,q1,q2,q4,q5");
        writer.println("a,b");
        writer.println("q0");
        writer.println("q0,q1,q2");
        writer.println("q1,q2");
        writer.println("q2,q5");
        writer.println("q2,q4");
        writer.println("q2,q4");
        writer.println("q0,q4");
        writer.close();
    }

    // Processes user's input string, prints output to the screen
    public static void processString(String[] input) {

        // For each element of the input string
        for (int i = 1; i < input.length; i++) {
                // Print the elements scanned so far
                for (int j = 0; j < i; j++) {
                    System.out.print(input[j]);
                }

                // Print spaces for formating
                for (int j = 0; j <= input.length - i; j++) {
                    System.out.print(" ");
                }

                // Print current state name
                System.out.print(current.name);

                System.out.print(" ");
                System.out.print("--");
                System.out.print(" ");

                // Print current element to scan
                System.out.print(input[i]);

                System.out.print(" ");
                System.out.print("-->");
                System.out.print(" ");

                // Input current element into transition function for current
                //  state. Then, change current state to the result of that.
                String newC = current.transitionFunction(input[i], alphabet);
                for (State state : dfa) {
                    if (state.name.equals(newC)) {
                        current = state;
                        break;
                    } else {
                        continue;
                    }
                }

                // Print updated current state name
                System.out.print(current.name);

                System.out.print(" ");

                // Print elements left to scan
                for (int j = i + 1; j < input.length; j++) {
                    System.out.print(input[j]);
                }

                // Print new line character
                System.out.print("\n");

        }

    }

    public static void main(String[] args) throws FileNotFoundException,
                                                UnsupportedEncodingException {

        // If args.length == 1, default to your minimal DFA.
        // If args.length == 2, use DFA profided in file listed at args[1].

        if (args.length == 1) {
            createDefaultDFAFile();
            File file = new File("minDFA.txt");
            buildDFA(file);
        } else if (args.length == 2) {
            File file = new File(args[1]);
            buildDFA(file);
        }

        // Now DFA has been constructed, begin to process input string.
        String[] input = args[0].split("");
        processString(input);

        // If DFA is in accept state at end of processing, print "accepted"
        if (current.isAccept) {
            System.out.println("accepted");
        } else {
            System.out.println("rejected");
        }

    }

}
